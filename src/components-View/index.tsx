import { FC } from "react";
import { useHistory } from "react-router";
// import { Link } from "react-router-dom";
import { List } from "./ViewHome/components/List";

const products = [
  {
    name: "Hamburger",
    price: 15,
  },
  {
    name: "Coca cola",
    price: 5.3,
  },
  {
    name: "Chips",
    price: 1,
  },
];

export const ViewHome: FC = () => {
  const history = useHistory()
  const handleOnClick = () => history.push('/checkout')

  return (
    <>
      <List products={products} />
      <button onClick={handleOnClick} >Checkout</button>

      {/* <Link to="/checkout">Checkout</Link> */}
    </>
  )
}
