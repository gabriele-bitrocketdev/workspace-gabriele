import { FC, useState } from "react";
import { Button } from "../../../../components-UI/Button";

interface IProps {
  products: IProduct[];
}

interface IProduct {
  name: string;
  price: number;
}

export const List: FC<IProps> = ({ products }) => {
  const [cart, setCart] = useState<IProduct[]>([]);
  const [counter, setCounter] = useState(0);

  const handleOnAdd = (el: IProduct) => {
    const newArray = [...cart, el];
    setCart(newArray);
    setCounter(counter + el.price);
  };

  const handleOnRemove = (productToDelete: IProduct) => {
    const filteredCart = cart.filter((el) => el.name !== productToDelete.name)
    setCart(filteredCart);
    setCounter(counter - productToDelete.price)
  }

  const handleOnReset = () => {
    const resetCartValue = setCounter(0);
    const resetCartText = setCart([]);
  }

  const listObj = products.map((el, index) => {
    const handleOnClickAdd = () => handleOnAdd(el);
    const handleOnClickRemove = () => handleOnRemove(el);

    return (
      <div key={index}>
        <li>
          {el.name}:{el.price}
          <button onClick={handleOnClickAdd}>+</button>
          <button onClick={handleOnClickRemove}>-</button>
        </li>
        <hr />
      </div>
    );
  });

  const shoppingBag = cart.map((el) => el.name).join(",");

  return (
    <div className="list">
      <h1>Scegli cosa prendere:</h1>
      <ul>{listObj}</ul>
      <p>{shoppingBag}</p>
      <p>{counter}</p>
      <button onClick={handleOnReset}>reset</button>
      <Button/>
    </div>
  );
};
