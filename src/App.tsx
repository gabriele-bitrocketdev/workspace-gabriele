import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { ViewHome } from "./components-View";
import { ViewCheckout } from "./components-View/ViewCheckout";



const App = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/checkout">
        <ViewCheckout />
      </Route>
      <Route exact path="/">
        <ViewHome/>
      </Route>
      <Redirect to='/' />
    </Switch>
  </BrowserRouter>

)

export default App;
